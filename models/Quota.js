var quota,
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Schema = mongoose.Schema,
  objectId = Schema.Types.ObjectId;

quota = new Schema({
  quota_name: {
    type: 'String',
    required: true
  },
  quota_price: {
    type: Number,
    required: true
  },
  quota_description: {
    type: 'String',
    required: true
  }
});

module.exports = mongoose.model('Quota', quota);
