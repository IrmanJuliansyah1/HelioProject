var validator = require('validator');

var user,
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Schema = mongoose.Schema,
  objectId = Schema.Types.ObjectId;

user = new Schema({
  fullname: {
    type: 'String',
    required: true
  },
  email: {
    type: 'String',
    required: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email',
      isAsync: false
    }
  },
  phone_number: {
    type: 'String',
    required: true
  },
  password: {
    type: 'String',
    required: true
  },
  role: {
    type: 'String',
    required: true
  },
  status: {
    type: 'String',
    required: true
  },
  company_id: {
    type: objectId,
    ref: 'Company'
  },
  quota_id: {
    type: objectId,
    ref: 'Quota'
  }
});

module.exports = mongoose.model('User', user);
