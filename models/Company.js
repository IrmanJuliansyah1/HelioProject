var company,
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  objectId = Schema.Types.ObjectId;

company = new Schema({
  company_domain_name: {
    type: 'String',
    required: true
  },
  company_name: {
    type: 'String',
    required: true
  },
  company_address: {
    type: 'String',
    required: true
  },
  company_logo: {
    type: 'String',
    required: true
  },
  company_phone_number: {
    type: 'String',
    required: true
  },
/*quota_id: {
  type: objectId,
  ref: 'Quota'
}
*/
});

module.exports = mongoose.model('Company', company);
