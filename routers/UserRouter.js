var express = require('express');
var router = express.Router();
var userLogic = require('../logics/UserLogic');
var companyLogic = require('../logics/CompanyLogic');
var quotaLogic = require('../logics/QuotaLogic');
var passport = require('passport');
var jwt = require('jsonwebtoken');

router.get('/users', passport.authenticate('bearer', {
  session: false
}), function(req, res) {
  var token = req.headers.authorization.split(" ")[1];
  var decoded = jwt.decode(token);
  userLogic.findAllUser(decoded.company_id, function(users) {
    return res.json(users);
  });
});

router.post('/users', function(req, res) {
  companyLogic.saveCompany(req.body, function(company) {
    quotaLogic.saveQuota(req.body, function(quota) {
      userLogic.saveUser(req.body, company._id, quota._id, function(user) {
        return res.json(user);
      });
    });
  });
});

router.post('/login', function(req, res) {
  userLogic.findUserForLogin(req.body, function(user) {
    if (user.authenticate) {
      var token = jwt.sign({
        company_id: user.company_id
      }, 'secret');
      return res.json({
        'success': true,
        token: token
      });
    } else {
      return res.json({
        'info': 'error authenticate',
        'success': false
      });
    }
  });
});

module.exports = router;
