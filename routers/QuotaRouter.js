var express = require('express');
var router = express.Router();
var quotaLogic = require('../logics/QuotaLogic');
var passport = require('passport');
var jwt = require('jsonwebtoken');

router.get('/quotas', passport.authenticate('bearer', {
  session: false
}), function(req, res) {
  quotaLogic.findAllQuota(function(quotas) {
    return res.json(quotas);
  });
});

router.post('/quotas', function(req, res) {
  quotaLogic.saveQuota(req.body, function(quota) {
    return res.json(quota);
  });
});

router.put('/quotas/:id', function(req, res) {
  var params = req.body;
  var id = req.params.id;

  quotaLogic.editQuota(id, params, function(data) {
    res.json(data);
  });
});

router.delete('/quotas/:id', function(req, res) {
  var id = req.params.id;

  quotaLogic.deleteQuota(id, function(data) {
    res.json(data);
  });
});
module.exports = router;
