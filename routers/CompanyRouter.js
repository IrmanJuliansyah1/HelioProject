var express = require('express');
var router = express.Router();
var companyLogic = require('../logics/CompanyLogic');
var quotaLogic = require('../logics/QuotaLogic');
var passport = require('passport');
var jwt = require('jsonwebtoken');

router.get('/companys', passport.authenticate('bearer', {
  session: false
}), function(req, res) {
  companyLogic.findAllCompany(function(companys) {
    return res.json(companys);
  });
});

router.post('/companys', function(req, res) {
  companyLogic.saveCompany(req.body, function(company) {
    return res.json(company);
  });
});

router.put('/companys/:id', function(req, res) {
  var params = req.body;
  var id = req.params.id;

  companyLogic.editCompany(id, params, function(data) {
    res.json(data);
  });
});

router.delete('/companys/:id', function(req, res) {
  var id = req.params.id;

  companyLogic.deleteCompany(id, function(data) {
    res.json(data);
  });
});

module.exports = router;
