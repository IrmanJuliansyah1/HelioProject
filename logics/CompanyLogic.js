const Company = require('../models/Company');

var companyLogic = {
  findAllCompany: function(callback) {
    Company.find(function(err, companys) {
      if (err) {
        return callback(err);
      }

      callback(companys);
    });
  },
  saveCompany: function(params, callback) {
    var company = new Company({
      company_domain_name: params.company_domain_name,
      company_name: params.company_name,
      company_address: params.company_address,
      company_logo: params.company_logo,
      company_phone_number: params.company_phone_number
    });

    company.save(function(err, companyS) {
      if (err) {
        return callback(err);
      }
      callback(companyS);
    });
  },

  editCompany: function(id, params, callback) {
    Company.findById(id, function(err, company) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      company.company_domain_name = params.company_domain_name;
      company.company_name = params.company_name;
      company.company_address = params.company_address;
      company.company_logo = params.company_logo;
      company.company_phone_number = params.company_phone_number;

      company.save();

      callback({
        status: true,
        message: 'Perubahan data berhasil.'
      });
    });
  },

  deleteCompany: function(id, callback) {
    Company.remove({
      _id: id
    }, function(err, company) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      callback({
        status: true,
        message: 'Company berhasil dihapus.'
      });
    });
  }

};

module.exports = companyLogic;
